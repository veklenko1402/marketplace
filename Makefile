.PHONY: start stop restart \
	composer-install dump-autoload \
	node-install node-build node-watch \
	cache-clear good build \
	ssh ssh-su ssh-node \
	migrate seed truncate\
	keygen \
	permissions

DC := docker-compose exec
FPM := $(DC) php-fpm
FPM_SU := $(DC) --user=root php-fpm
NODE := $(DC) node yarn
ARTISAN := $(FPM) php artisan

start:
	@docker-compose up -d

stop:
	@docker-compose stop

restart: stop start

composer-install:
	@$(FPM) composer install

dump-autoload:
	@$(FPM) composer dump-autoload

node-install:
	@$(NODE) install

node-build:
	@$(NODE) run build

node-watch:
	@$(NODE) run watch

good: permissions cache-clear composer-install node-install node-build migrate seed

cache-clear:
	@$(ARTISAN) cache:clear
	@$(ARTISAN) config:clear
	@$(ARTISAN) route:clear

build:
	@docker-compose build --no-cache

ssh:
	@$(FPM) bash

ssh-node:
	@$(DC) --user=root node bash

ssh-su:
	@$(FPM_SU) bash

migrate:
	@$(ARTISAN) migrate

seed:
	@$(ARTISAN) db:seed

truncate:
	@$(ARTISAN) db:wipe

keygen:
	@$(ARTISAN) key:generate

permissions:
	@$(FPM_SU) chmod 777 -R .
